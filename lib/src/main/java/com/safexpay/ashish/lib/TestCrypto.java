package com.safexpay.ashish.lib;

//import android.org.apache.commons.codec.binary.Base64;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Arrays;
import java.util.Base64;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


public class TestCrypto {
    //region initializing
    public static final String internalKey = "HiktfH0Mhdla4zDg0/4ASwFQh2OS+nf9MVL0ik3DsmE=";
    private static final String ENCRYPTION_IV = "0123456789abcdef";
    private static final String PADDING = "AES/CBC/PKCS5Padding";
    private static final String ALGORITHM = "AES";
    private static final String CHART_SET = "UTF-8";
    private static final String CLASS_NAME = "javax.crypto.JceSecurity";

    public static Map<String, String> splitQuery(URL url) throws UnsupportedEncodingException {
        Map<String, String> query_pairs = new LinkedHashMap<String, String>();
        String query = url.getQuery();
        String[] pairs = query.split("&");
        for (String pair : pairs) {
            int idx = pair.indexOf("=");
            query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
        }
        return query_pairs;
    }

    //endregion
    public static void main(String[] arg) throws NoSuchAlgorithmException {
        //region crypto lib class path static method
        {
            try {
                final Field field = Class.forName(CLASS_NAME).getDeclaredField(
                        "isRestricted");
                field.setAccessible(true);
                field.set(null, Boolean.FALSE);
            } catch (final Exception ex) {
                ex.printStackTrace();
            }
        }
        //endregion

        BigDecimal a = new BigDecimal("10.010001");
        BigDecimal b = new BigDecimal("20.11");

        double t = a.doubleValue() + b.doubleValue();
        @SuppressWarnings("DefaultLocale") String fv = String.format("%.2f", t);
        double ev = Double.parseDouble(fv);

        System.out.println("BigDecimal total :" + ev);

        //region txn details
        /*System.out.println("order no " + encrypt("33348", "KXjxCR5OagliWDjCF444iEyUPZg08QPtTBq6JPhDTiA="));
        System.out.println("ag_ref " + encrypt("1028234909435932672", "KXjxCR5OagliWDjCF444iEyUPZg08QPtTBq6JPhDTiA="));
        System.out.println("result " + decrypt("UOGEnewk+7Z+fJZ2Mj3hJIipuiG0Ovo5ma0WEYqMP5LFhISYs4/ddoMhPtub58VYyrZ0wjvoawd1wa0v8Zfp+lWwdmt75HK4EVSOZw2lcdRUfIb4RRfH/IzQd8saFh/H6ipOUPZJ5DKpT5fGoMdLryGifcoO/BMKP5GzGnnW2PBNRpX8FdA0MAr7FKivOYe46KaH44YfY4eq5nWtl/JzIFgmcplCR8+1J3mQ1BDaBsUv3MfgtksCeQhHghFu7/7a+aydNTV6HCewUGuc6raa3RudKdM5FJHSoqW9WoTuAQ2rbe8PkUrP7Mt2Pd4PXEJLqzOoMRyucd3ydq3mR10vzz4oyvcu59xCL9yzKB8LnCmbkoxQahdKYDvYPu2fz33I5E5E9pC+eCOeq2AhCreuEP++Roe/2LHRBn5bGMOi6B09zJ5x/paUWjmiINgJppYC8fbdXBHpJ5/qSc/1/ZIDJCc5jEAnj2dcIDt4YmHJhqLCbFFiMZDi+jcvrDtWjM6xUNUYGeIJHAfzmQmBqRanmLEY9cc4+ppJqiQPU5f9Dm3OCXyr6paSI9b1OVaIl2haKfRb1OLEd9iMB+KO6492xw==", "KXjxCR5OagliWDjCF444iEyUPZg08QPtTBq6JPhDTiA="));

        System.out.println("tnx : " + decrypt("e18F1wDogkR8xAwtVB1VqKGCnnBDxCS3R8AEgi8MGx3368XK7IG+frIg5PPfneGg/qss06ZSU7lXhQWBIU8h5uRKh0+NU579+EMWwH3L/NCUGYlfsbeF7kI1BD/x19FDfKiEs77QkYbF7C8h22M4cw==", "vclXfe8niQnDzsP77uleoXygJjGzjnrxAWqaKLBQ05Q="));
        System.out.println("pg : " + decrypt("aYBcP1ltvcY6ilUk8B5ASQ==", "vclXfe8niQnDzsP77uleoXygJjGzjnrxAWqaKLBQ05Q="));
        System.out.println("card : " + decrypt("epGE18wD+Brt+B6+XorJEQ==", "vclXfe8niQnDzsP77uleoXygJjGzjnrxAWqaKLBQ05Q="));
        System.out.println("cust : " + decrypt("5MiJkq2UH6Qwm/5i+35u6OvnWMhtlz+7GelB3zT+bJ0RUmxumMQGQ9Zd2xsHEVzg", "vclXfe8niQnDzsP77uleoXygJjGzjnrxAWqaKLBQ05Q="));
        System.out.println("bill : " + decrypt("epGE18wD+Brt+B6+XorJEQ==", "vclXfe8niQnDzsP77uleoXygJjGzjnrxAWqaKLBQ05Q="));
        System.out.println("ship : " + decrypt("N380xkIpVs1YPSzK/yBqWw==", "vclXfe8niQnDzsP77uleoXygJjGzjnrxAWqaKLBQ05Q="));
        System.out.println("item : " + decrypt("aRpzuzBtWYk0PHro4p1g2A==", "vclXfe8niQnDzsP77uleoXygJjGzjnrxAWqaKLBQ05Q="));
        System.out.println("other : " + decrypt("epGE18wD+Brt+B6+XorJEQ==", "vclXfe8niQnDzsP77uleoXygJjGzjnrxAWqaKLBQ05Q="));


        System.out.println("tnx : " + decrypt("fYUsol2JrMJlREr9e9adQkozMizNmitNIxCOahrli427QvwIHjGwb1XICacirLQDlMHiiao9LHwiMtPckU6/1COgnnG0Fl7EEZf3IIEyaMqze0ycitjZfg36f4Qr5jDe2RQ8Ck6m0TzxsVnOw7YBhEkypekZK0UEy2tH1lhbTEqmyd9d9IbHJqhLwhvno+yDIwZH5+axWSY7To0NYebz/NIdtoInKUGSwPX25lW5C/4=", "KXjxCR5OagliWDjCF444iEyUPZg08QPtTBq6JPhDTiA="));
        System.out.println("pg : " + decrypt("ayja1zET8l6KzjKaI2zFTQ==", "KXjxCR5OagliWDjCF444iEyUPZg08QPtTBq6JPhDTiA="));
        System.out.println("card : " + decrypt("wA5OXfCRt+3TZX3atMMg2A==", "KXjxCR5OagliWDjCF444iEyUPZg08QPtTBq6JPhDTiA="));
        System.out.println("cust : " + decrypt("aT5DFZmngxPzn3MFG3hezA==", "KXjxCR5OagliWDjCF444iEyUPZg08QPtTBq6JPhDTiA="));
        System.out.println("bill : " + decrypt("wA5OXfCRt+3TZX3atMMg2A==", "KXjxCR5OagliWDjCF444iEyUPZg08QPtTBq6JPhDTiA="));
        System.out.println("ship : " + decrypt("TXkOT6ArFWUFuxaClPOxKQ==", "KXjxCR5OagliWDjCF444iEyUPZg08QPtTBq6JPhDTiA="));
        System.out.println("item : " + decrypt("uHEKlNmVyNWWsQiqUlAKwA==", "KXjxCR5OagliWDjCF444iEyUPZg08QPtTBq6JPhDTiA="));
        System.out.println("other : " + decrypt("W65aUGcxpnMOIoEyrQdMdw==", "KXjxCR5OagliWDjCF444iEyUPZg08QPtTBq6JPhDTiA="));
        //System.out.println("upi : "+decrypt("lW/jUhBS2Dqx2I49h+WHXg==","PE5K0s/KBPfwyxRDxWW64nRxdYMHIaAuQIRBCl7LyaE="));*/

        //endregion

        //region also details

        /*String url = "https://test.vastwebindia.com/Response/GatewayOutput?txn_response=Mk%2F1gPvwxgqanVL8u2vnRDsbgclbL%2FJFMNqFkOIJ4Ep5lWUdK1q6DpJeaD2nGOafeakLItn6AoS3lsoLYSIa1%2Bf5lR8NLIyununystgh4mtNVb%2BpdfJTfhwu4NgK6uqtdA7jehoU0xqkHuZF7dhvQggHUy6jtEDdR2eV5Sj1YIZAUw0uHmCgGg3%2FEHX5Ygzr&pg_details=%2FNaFAPCeHPnbJlvHY68QpA%3D%3D&fraud_details=t8gV4zyBFHM5njeBqPik9g%3D%3D&other_details=0QxlGTEG1xuTuQgr00q2JQ%3D%3D&me_id=202105170004&txn_details=Mk%2F1gPvwxgqanVL8u2vnRDsbgclbL%2FJFMNqFkOIJ4EomUG9L5zMjqydPolSwFcnBi0uuhwqHXuhP9oVKk6RXJC7A7bKDBWU%2B4VY%2FWklMaWNMittzbWxmf01I5uaYA8o5o76zZD61WuU%2BE6s1dkRxvPaVvC6%2BaAVZsx1t88se5%2B1h0XgYGDCqXjorL6j8axP3bwIDrh8Rdw%2FKcgIXyAEycg%3D%3D&card_details=KqjktBcksYLaOJnX82ObRw%3D%3D&cust_details=qbitvH0E%2BFWq7AtlHRdnVwJeJwXBcx6FJcTDGzZ%2FOG8Rn7pG52wAOtAL6f1RzhYL&bill_details=0QxlGTEG1xuTuQgr00q2JQ%3D%3D&ship_details=zJ8JppfKEg%2F7LC0vTAux2A%3D%3D&item_details=1TGWYdnxC6y21vQO0hpftA%3D%3D&retryPayment=N&logoUrl=&aggLogoUrl=https%3A%2F%2Fwww.avantgardepayments.com%2Fagres";
        Map<String, String> map = null;
        try {
            map = splitQuery(new URL(url));
        } catch (UnsupportedEncodingException | MalformedURLException e) {
            e.printStackTrace();
        }
        if (map != null) {
            String txn_response = map.get("txn_response");
            String pg_details = map.get("pg_details");
            String fraud_details = map.get("fraud_details");
            String other_details = map.get("other_details");
            String me_id = map.get("me_id");
            String txn_details = map.get("txn_details");
            String card_details = map.get("card_details");
            String cust_details = map.get("cust_details");
            String bill_details = map.get("bill_details");
            String ship_details = map.get("ship_details");
            String item_details = map.get("item_details");
            String retryPayment = map.get("retryPayment");
            String logoUrl = map.get("logoUrl");
            String aggLogoUrl = map.get("aggLogoUrl");

            System.out.println("txn_response: " + txn_response);
            System.out.println("pg_details: " + pg_details);
            System.out.println("fraud_details: " + fraud_details);
            System.out.println("other_details: " + other_details);
            System.out.println("me_id: " + me_id);
            System.out.println("txn_details: " + txn_details);
            System.out.println("card_details: " + card_details);
            System.out.println("cust_details: " + cust_details);
            System.out.println("bill_details: " + bill_details);
            System.out.println("ship_details: " + ship_details);
            System.out.println("item_details: " + item_details);
            System.out.println("retryPayment: " + retryPayment);
            System.out.println("logoUrl: " + logoUrl);
            System.out.println("aggLogoUrl: " + aggLogoUrl);

            String dTnx = decrypt(txn_response, "/LrlYjgfoMpANx/HCpAXkhHzNilDcrm9oUMA/lgXV/8=");
            String[] aa = dTnx.split("\\|");
            System.out.println("\n\ntxn_response length: " + aa.length);
            System.out.println("\ntxn_response agg: " + aa[0]);
            System.out.println("\ntxn_response mei_d: " + aa[1]);
            System.out.println("\ntxn_response pid: " + aa[9]);
            System.out.println("\ntxn_response msg: " + aa[12]);
            System.out.println("\ntxn_response msg: " + Arrays.toString(aa));


            System.out.println("\n\ntxn_response: " + decrypt(txn_response, "/LrlYjgfoMpANx/HCpAXkhHzNilDcrm9oUMA/lgXV/8="));
            System.out.println("pg_details: " + decrypt(pg_details, "/LrlYjgfoMpANx/HCpAXkhHzNilDcrm9oUMA/lgXV/8="));
            System.out.println("fraud_details: " + decrypt(fraud_details, "/LrlYjgfoMpANx/HCpAXkhHzNilDcrm9oUMA/lgXV/8="));
            System.out.println("other_details: " + decrypt(other_details, "/LrlYjgfoMpANx/HCpAXkhHzNilDcrm9oUMA/lgXV/8="));
            System.out.println("me_id: " + me_id);
            System.out.println("txn_details: " + decrypt(txn_details, "/LrlYjgfoMpANx/HCpAXkhHzNilDcrm9oUMA/lgXV/8="));
            System.out.println("card_details: " + decrypt(card_details, "/LrlYjgfoMpANx/HCpAXkhHzNilDcrm9oUMA/lgXV/8="));
            System.out.println("cust_details: " + decrypt(cust_details, "/LrlYjgfoMpANx/HCpAXkhHzNilDcrm9oUMA/lgXV/8="));
            System.out.println("bill_details: " + decrypt(bill_details, "/LrlYjgfoMpANx/HCpAXkhHzNilDcrm9oUMA/lgXV/8="));
            System.out.println("ship_details: " + decrypt(ship_details, "/LrlYjgfoMpANx/HCpAXkhHzNilDcrm9oUMA/lgXV/8="));
            System.out.println("item_details: " + decrypt(item_details, "/LrlYjgfoMpANx/HCpAXkhHzNilDcrm9oUMA/lgXV/8="));
            System.out.println("retryPayment: " + retryPayment);
            System.out.println("logoUrl: " + logoUrl);
            System.out.println("aggLogoUrl: " + aggLogoUrl);

            System.out.println("\npg: " + decrypt("i+8whUlQyhwMV9B65y2TYg==", "/LrlYjgfoMpANx/HCpAXkhHzNilDcrm9oUMA/lgXV/8="));
        }*/

        //endregion

        //region testing details

        /*System.out.println("PG_DETAILS "+decrypt("A9gvv+goEcSkmua8tM6Tug==", "/LrlYjgfoMpANx/HCpAXkhHzNilDcrm9oUMA/lgXV/8="));
        System.out.println("CARD_DETAILS "+decrypt("0QxlGTEG1xuTuQgr00q2JQ==", "/LrlYjgfoMpANx/HCpAXkhHzNilDcrm9oUMA/lgXV/8="));
        System.out.println("CUST_DETAILS "+decrypt("qbitvH0E+FWq7AtlHRdnVwJeJwXBcx6FJcTDGzZ/OG/t+ZQPHJ5o8zaPXItMviCZ", "/LrlYjgfoMpANx/HCpAXkhHzNilDcrm9oUMA/lgXV/8="));
        System.out.println("BILL_DETAILS "+decrypt("0QxlGTEG1xuTuQgr00q2JQ==", "/LrlYjgfoMpANx/HCpAXkhHzNilDcrm9oUMA/lgXV/8="));
        System.out.println("SHIP_DETAILS "+decrypt("rJEFbrXn+CIGX/7+PltvoQ==", "/LrlYjgfoMpANx/HCpAXkhHzNilDcrm9oUMA/lgXV/8="));
        System.out.println("ITEM_DETAILS "+decrypt("2nfqh7L+J8uuYnZPfsX3xQ==", "/LrlYjgfoMpANx/HCpAXkhHzNilDcrm9oUMA/lgXV/8="));
        System.out.println("OTHER_DETAILS "+decrypt("0QxlGTEG1xuTuQgr00q2JQ==", "/LrlYjgfoMpANx/HCpAXkhHzNilDcrm9oUMA/lgXV/8="));

        System.out.println("pg_details : "+decrypt("zteHH6PYK1HUFSf8X+7iasY7+CjiVbGDMLGx+3b4I6Q=","PE5K0s/KBPfwyxRDxWW64nRxdYMHIaAuQIRBCl7LyaE="));*/

        /*System.out.println("txn : " + decrypt("Mk/1gPvwxgqanVL8u2vnRM85Tcw2JfVKgzxgiIcS8yfEXjVujTfmx4U+MfYkb7bSmXCMvTkDqFYslcRXbEuWc0b7d+JUTuvC6ZhPClHiAISYd49VcMG05n//Vc9KHVUnneheTjOx02ReRRRm8kRXzcen54F1FHI5o+JpOFyDw3g=", "/LrlYjgfoMpANx/HCpAXkhHzNilDcrm9oUMA/lgXV/8="));

        System.out.println("pg : " + decrypt("/NaFAPCeHPnbJlvHY68QpA==", "/LrlYjgfoMpANx/HCpAXkhHzNilDcrm9oUMA/lgXV/8="));
        System.out.println("other : " + decrypt("0QxlGTEG1xuTuQgr00q2JQ==", "/LrlYjgfoMpANx/HCpAXkhHzNilDcrm9oUMA/lgXV/8="));
        System.out.println("frd : " + decrypt("SL5LFGTkbDJiwNm4mVSllg==", "/LrlYjgfoMpANx/HCpAXkhHzNilDcrm9oUMA/lgXV/8="));

        System.out.println("PG_DETAILS "+decrypt("A9gvv+goEcSkmua8tM6Tug==", "/LrlYjgfoMpANx/HCpAXkhHzNilDcrm9oUMA/lgXV/8="));
        System.out.println("CUST_DETAILS "+decrypt("qbitvH0E+FWq7AtlHRdnVwJeJwXBcx6FJcTDGzZ/OG/t+ZQPHJ5o8zaPXItMviCZ", "/LrlYjgfoMpANx/HCpAXkhHzNilDcrm9oUMA/lgXV/8="));
        System.out.println("CUST_DETAILS "+decrypt("qbitvH0E+FWq7AtlHRdnVwJeJwXBcx6FJcTDGzZ/OG/t+ZQPHJ5o8zaPXItMviCZ", "/LrlYjgfoMpANx/HCpAXkhHzNilDcrm9oUMA/lgXV/8="));*/

        /*System.out.println(encrypt("33348",internalKey));
        System.out.println(encrypt("98344741-ff3e-4974-9b1f-5e46141706f6",internalKey));
        System.out.println(encrypt("1028234909435932672",internalKey));

        System.out.println("order no "+encrypt("18126","KXjxCR5OagliWDjCF444iEyUPZg08QPtTBq6JPhDTiA="));
        System.out.println("ag_ref "+encrypt("1025757875669413888","KXjxCR5OagliWDjCF444iEyUPZg08QPtTBq6JPhDTiA="));
        System.out.println("result "+decrypt("UOGEnewk+7Z+fJZ2Mj3hJIipuiG0Ovo5ma0WEYqMP5LFhISYs4/ddoMhPtub58VYyrZ0wjvoawd1wa0v8Zfp+jLc3pfa2wyfr0qqxvBWDqgN97bQS7VaA3dl8PPWbx8LnDKQu+Efsmpu10oCYto+wihjTulrJzomI6Fpcsm4ryd4lmU0Z9HD20QyOvSfMZJGGiKo/xi2giNxE0pWWENE5wZLyyY6Xrm8nz327sWNa6I9Q6Vc58RFu5eOTChVwj47OKahOlpXjlDlHKIPEm9E2fpgPMieTj1SZ8eKNYrXXkUJs5XJXy0b4WkMSkaIkt0Ma7gCrMmpbQpMpk3j/4jsIyCXp20Ra65giqrzw7FnWYBcimg9B3ryIvbKqZVwgCaRJHj6zp7W2zUN1ZiRzZcff3mVavcbJXV+fvMwRU1W+BxOwKpEYmL7WqGAQvn06bIc/zBN4DfLboEDvuWv74Zneq8deNb5k985LpEYTtcIawxIjK8lePk19btW4kH/o24GWV3mfBMPNVuM2+QAclFaRBs9K11Rg/TXXX0z6FT8d5OFPzZ2ifTcGuL/unS7PFlrQrxUIHyfZrysqBQ5rcgXG9Q1cT0q4X6OhcqGV6WQmI3gIZ+yal3yUclXYtB+ncDvd8NaT/ENatuBr7CXHUVhIOk7VgtnguHQepGFu1uF2F8=","KXjxCR5OagliWDjCF444iEyUPZg08QPtTBq6JPhDTiA="));

        System.out.println("result "+decrypt("Mk/1gPvwxgqanVL8u2vnRBjG0pOFRkfy9BWkE6wp86jrbpvP3M3+jpfSGYvB7fpi+YL38L9NXrqPSj+O1cDFhF1dX3JIkNwxscHI3fLg7eDMCv2WucGFDbnY4eaGUbiNHcw0zOO5v86s/TgjqvZGsahIyER+pp53/4ZD7u1A/A4=","/LrlYjgfoMpANx/HCpAXkhHzNilDcrm9oUMA/lgXV/8="));*/

        /*System.out.println(encrypt("025953","PE5K0s/KBPfwyxRDxWW64nRxdYMHIaAuQIRBCl7LyaE="));
        System.out.println(encrypt("1025409738370343068","PE5K0s/KBPfwyxRDxWW64nRxdYMHIaAuQIRBCl7LyaE="));
        System.out.println(decrypt("76DWd91lLt4DksRzbZyquHJ1efR4uNKI3gCbI7gIO/hU/FejwMlnygMsA" +
                "JKazwWXiGHAuMInIvKOxLK0CHC8SQjDNg2Gl7HOw2JMPJ3t9QR5y3P0nv6sLyLPTarpcjwEKAJIxDShExee2G" +
                "6nWxxjZgSatdaa3qTUjRzua5XvAl++umr/LhTGPSuuS3OExZKUwyAT4nXMMz3LLlQyDVGDKnBPR3SJLFGchd" +
                "EI4J8fIyFwhpEf/TPKJ6+qMCKeOY4qzxpwj17GDwHQE18EKF3MQozPk4/Uv/55I4lrjB1lDU+yLz" +
                "9YCzy+6/OlnL6KRfKXNJJHiitOZk5kMlu23TxFVvQVdUUKA+qcrkAJAKkmFlpJdYKaUZGVIAyxdAQ9Zo3+" +
                "0ukG53/qe+qHi8SVkCZSHmfcTZ3W81sOLZihET31Zt4J/x7ARsIaz1y+4ITxIE51U654OnO6NtpbGvmwh" +
                "AmQ1oDhV9jJv28Qi5PopYG6ibzgjACz5+JytxFF29RtEjnEHMEm95i5H8zTm65ms090NvZvPaF5uhegjXw" +
                "BfgK2BIE2Va45rT/WN2AaX+MSC6C+aIf/Dv1w+I6kPG6jfgZNr6MCsw0/f1SdyGIbGIgSVcnCShj4xsv3fmdz" +
                "3Znm90eXCWx2WQSKL9Q/vi6UikcoRA==","PE5K0s/KBPfwyxRDxWW64nRxdYMHIaAuQIRBCl7LyaE="));*/

        /*System.out.println("Decrypted MID: "+decrypt(new String(Base64.getDecoder().decode("Q20wOEdsaWlERy9STVpnUHpaSFV6Zz09")), internalKey));
        System.out.println("Encrypted MID: "+new String(Base64.getEncoder().encode(encrypt("202105020006", internalKey).getBytes(StandardCharsets.UTF_8))));
        System.out.println("Decrypted ORD_NO: "+decrypt(new String(Base64.getDecoder().decode("WWt2SGQvVUVyUFAxYzY1RnA1Tk5qQT09")), internalKey));
        System.out.println("Encrypted ORD_NO: "+new String(Base64.getEncoder().encode(encrypt("43437", internalKey).getBytes(StandardCharsets.UTF_8))));*/
        //System.out.println("Encrypted ORD_NO: "+new String(Base64.getEncoder().encode(encrypt("48437", internalKey).getBytes(StandardCharsets.UTF_8))));


        //System.out.println(decrypt(new String(Base64.getDecoder().decode("Z1lPYXZ5OXVvcmtRaWtGM2VBekxzMGluM2o4Q3dwRTc4dENBakY4R0x5dz0=")),internalKey));
//        System.out.println(new String(Base64.getEncoder().encode(encrypt("424242",internalKey).getBytes(StandardCharsets.UTF_8))));
//        System.out.println(new String(Base64.getEncoder().encode(encrypt("202104300001",internalKey).getBytes(StandardCharsets.UTF_8))));
//        System.out.println(decrypt("0FskuKfn41hyQTun55PZ0ioGANx0TVEohcCXJJTW8pA=", internalKey));
//        System.out.println("\n status : "+decrypt("rnMO8OFVtLIVxqsWJkWR4wjKz0IMzftCV5yhXBKSx9qMW1N7nfB/Y6FlPzBtOj9dMam8L23fBXVBq4qKl4XK3u5ViaidTn26pCtPzl7OkaidR2Fyx8wRqeYDisLznscCC/JfqQiG+Gx7cgt/UHZqy4qjQ0vpjhRyJcX+jUoE/2Tuwm4CcPw0nIIOjrhbdzTrxv92ClvIwpLkr3pLpJuHKIqzv/LFmi6NHdgeVnoRv3v4w64Eqmyzban3S9juzj8r8mwg2RNETzQb4moY9l8pdh9nxI/kegi5jM/RZf+As0YyumeQb0BVyC6CkR0agkyCCqBnqG19/tgxn7n48NtfRXinrOsKFedwtDi55F/9Zj1BJUgWRRKihIZ3BGWkHmM/0sKCLG2a+R55pbFURDBrv1oAfgFP0/HDAx0359nbVN4B7FKIToY5zRWM7+ikt50W0Z+Fwl0tKbKcU7AouqAmDy/qeoQrMZPv8+sI6YlVmj+h3+jlkIKTIdlYbNAYe/7Q2HrLQGAnLzFs0RVSrNK7jqu3t+6P7zlVqagf/IRSCRyoUlkw21QtoAoE1RcrmUZf+Df3vokG+z7wqzuAznLOcVTAhF+iQ7IbvlzD3vYTDvCo9QSdSuGJ7mOFVYKeWp7IsrI/ImIZ2duB+W2VlCMPDEGEPVb+5dMGzhngLZE2wsu6hzI8H1I4SkNKn19Dhk+nTPKLaRd9lkKsEj2ZeBk9VPsMeC8rkJrsGjhXc52jvQOJSYBP7hGJDLBlrHjyOd5j", internalKey)+"\n");
        //System.out.println("MID: "+new String(Base64.getDecoder().decode(decrypt("WWt2SGQvVUVyUFAxYzY1RnA1Tk5qQT09", internalKey).getBytes(StandardCharsets.UTF_8))));
        /*System.out.println("\n merchant_code : "+ decrypt("Y5a3eObyR8U1Rdh8YRlnyA==","OTZbW7JEd7zPXtYl3O62ij/q+S8fq3FNTRWJS4UlSkI="));
        System.out.println("\n ag_ref : "+ decrypt("OyDz19qGVcySffvq7Z0feQYUqyb1RXlNpAXOZDAUqvQ=","OTZbW7JEd7zPXtYl3O62ij/q+S8fq3FNTRWJS4UlSkI="));
        System.out.println("\n ag_ref : "+ decrypt("yOw2K+5R911/jI2z8Q/eHIcMdv5WspC98Yw2EPJdRnaIzaniSntzIq0mV9yaiBAT","OTZbW7JEd7zPXtYl3O62ij/q+S8fq3FNTRWJS4UlSkI="));*/

        //System.out.println("\n ag_ref : "+ decrypt("Z1lPYXZ5OXVvcmtRaWtGM2VBekxzMGluM2o4Q3dwRTc4dENBakY4R0x5dz0=",internalKey));
        /*System.out.println("\n ag_ref : "+ encrypt("4200000000000000","OTZbW7JEd7zPXtYl3O62ij/q+S8fq3FNTRWJS4UlSkI="));
        System.out.println("\n ag_ref : "+ encrypt("4200000000000000",internalKey));*/
        /*System.out.println("\n ag_ref : "+ decrypt("OyDz19qGVcySffvq7Z0feQYUqyb1RXlNpAXOZDAUqvQ=",internalKey));
        System.out.println("\n ag_ref : "+ decrypt("Y5a3eObyR8U1Rdh8YRlnyA==",internalKey));*/

        //System.out.println("\n txn_details : " + encrypt("fab|202104300001|612202098a|1|ARE|AED|SALE|http://localhost/safexpay/response.php|http://localhost/safexpay/response.php|MOBILE", "OTZbW7JEd7zPXtYl3O62ij/q+S8fq3FNTRWJS4UlSkI="));
        /*System.out.println("\n pg_details : " + encrypt("4|PL|108|6", "OTZbW7JEd7zPXtYl3O62ij/q+S8fq3FNTRWJS4UlSkI="));
        System.out.println("\n cust_details : " + encrypt("Nagesh|successful.payment@tabby.ai|500000001||Y", "OTZbW7JEd7zPXtYl3O62ij/q+S8fq3FNTRWJS4UlSkI="));
        System.out.println("\n card_details : " + encrypt("||||", "OTZbW7JEd7zPXtYl3O62ij/q+S8fq3FNTRWJS4UlSkI="));
        System.out.println("\n billing_details : " + encrypt("||||", "OTZbW7JEd7zPXtYl3O62ij/q+S8fq3FNTRWJS4UlSkI="));
        System.out.println("\n shipping_details : " + encrypt("||||||", "OTZbW7JEd7zPXtYl3O62ij/q+S8fq3FNTRWJS4UlSkI="));
        System.out.println("\n item_details : " + encrypt("||", "OTZbW7JEd7zPXtYl3O62ij/q+S8fq3FNTRWJS4UlSkI="));
        System.out.println("\n other_details : " + encrypt("|||||", "OTZbW7JEd7zPXtYl3O62ij/q+S8fq3FNTRWJS4UlSkI="));*/


        /*double d = new BigDecimal(10).doubleValue();
        System.out.println("abc"+(d+10));*/
        //System.out.println(decrypt("Z1lPYXZ5OXVvcmtRaWtGM2VBekxzMGluM2o4Q3dwRTc4dENBakY4R0x5dz0=", internalKey));
        //System.out.println(decrypt("ZdSifF8fM8r5oTx2UKUnNQFtNj6G1KfVUYAwEok1l7BujYnb+/bIYrCqqW1BQ9iS8rEtqBsMAjUOg1AZsnDwO5SbklBOARQEketCAUn1b3VMIFYntd8QQDZZO9T6aSr0rO+etSvDRqyAWZfDAcOOJ4igT/iLElfnsLiGx+t/gszgZ4/ezQT53ooisJi2Gjnr", internalKey));

        /*System.out.println(decrypt("ONmItFNCxaALCPOuMbM2NaM+sp/VZrUl1nYt1RlNQsBVBqNnyw7SrrTOVIoX1girG4276r1/INSSz6S/rcEExezR/avqLM3y81oTQh2rOga/AVz+kEsd1AYuNsWtkZkpzDmi1JhKpn3EA08l9GALpTIHJGmFplRnbyomnsoaCBCfXXyAl58RV7Ka6PYC+TtDVoKTj/5gUyMMHMtxLh/rY1YVw9cLD0ZFjeodelny5MIgrkt3b3gB7NhAeKyPn97PcZJTjtY7fTidtBz3fL+3XijmPKKtAoUYxlU6Z68DQTWIumq2SwGVwKJvbu3B6+dJnZUnGpN5ewVjV7Qh3jUocJRlMzA8rf+36QO7oL9XNo1ul3SyFQ3Y8P8hgk4fu9PdytXxe03LxLH14qNLg9Gy1T7g83OLdwQsDee9pn9mVolOwDcvGdyemb7Ag6kTSzA1TjUCWk7FBR/lrjOTe4W0Jf9sv7QblKuJMESJ2su62aFXRMWDm9HUt0hQTsXeAPPYtlWQj4RCcnhCXne4nSNSiyRFD2VWezEgUHdAqWecs867FbgqnXln9EdUF8h4X+Z7ig2YgAQZTbzleRH43fQJR0hEegS3YwGooeDpT5AtyVXVf/l5ysalng4+Nkcfos1miNpEOQGj8mgUdRkDdND+IHQfUtUGok1aDB14Kzvu3rwlXDzSO1PS6ZtoYVqNwA1OIZ+Vy+/yETozB8x7X4dFFwwx8wdmoKRIY9ZUnnCp+/oBSDS064fa/s6+6A1CD0uOFIsqGIiidB4nYIJ39FjFMAbuoFoVz+UK84uyE4Oh2gRAAvvnTBFctgrJfrsoy39QcdGwPxjgniT+hyULKjbNIimmANtdnS5gipzZt4iLSckwaTwR0bOIMCz9golEwbqQ9d/dcE0yHmjDYT5GyPnVfmDpYiXvukhlutKT+lD4PMBdCmCO/T/pgKGN5dK9UyJhtHBCrhBXZoYN8OK6Pff0fsIED6rQICUVe8UrDBBdkPPl27Rwwxa4Gx2fIEfytAFVL6djNKWyDssVL4Xx0LUxnDhjc0TNbm4BQl9H2BajqaNZg1/RaK5pMfTZrIfwOfHHTmH0HLSD7WJv5N4ls4jCLj+zkwHf7y4FZ2bQwAETySPm0BjLRyaUacYremJUsx+HsjZUzU9/P4hmvm1WHUSawOUYf3rnwKmF+a8zVpd6DSGB5QMhbr2fYQzDvk97wWl4A4CDvA1C0G2sJronMA/uVWmdXexAeFyYc0de7V/ejUA36lDXMaalFZV7EbkeEnbWZxIeIdg5KHvdqVmcW9+6DVX8+SvVlfqJAT0bcUJwDgz1KrNBCRfi7HASRrna9slYiMpnKNih+FZMZospz/Zu7yT9IFip+kB8cGfkD45jdvvdeWW7rtmzkxEp8JdFA8t2OV6Zg2S9AhhN17KMPbCH/EP02t7sBaLYB55s6C7xHnPUnDAxktcNxShSqy9W/gM2YiYUHXRfyKRTr5axbe9dIXbZeGc0aNZY9+k1b/5BtmBib7i8VLMdQauLFodOHstT2UFQN5FH0Wv4O2OygDXWxKzUeOCAGHPHbfrAvHQRAnM=", internalKey));
        System.out.println(decrypt("9ftP0j8PgP86+cZQXkk+lSCbXNj79FSW/2YasSWFXqoqGvxwjDhOHXNVfW5XAvOIzJEUH/qAz/aV4JZea0M8kK+OF9Ta6eJp5q+RyAUQvtFJfHyx1Ls/0TGlwYcQKKse5dZ8+qoiMh0cj3BfIrT3uJwbl+1Knxyk7+rimEmbPhCBY/2oXJglhgWyNQYrv/qZVg03FTA9NiS2doCQCi/GZBZh9iBOZX57E+ujSTEHvhYH6sOYd/9cZ2Je5vACgFhgM/QjjQW4YQrieGgl9QrjeVa/kWkHN+lOlIMNEYqZC7cHphcHkZnBXml6iXf1taaVAWPaxJVNZjYIua6c6Q8FJcay7SI7+inicyNqkZwv/Q/q7NldEGzfnZydr8V4aVqu9tgEO4urb4MsD2zkJ9BWb8aB2I/f2qU5XwOoXg4B+3hMB/jHcyII1wmH3/GxIqvu3L8ojqdGiZDz8kBbNaDav0JIrwvUBx/3ixKq6T9D0bcFjRaQMaekumDuLfXJ21EVR+JT0UUZLA5F/kEtn/MKPatRuyayNULAMD8eyUxqg5I7Z5C7DlvejhpQT/ONsQkAYkcM9FQoxiX6n6k3pa3HfchswAwIlHT161JHCsRXUGgSKmjIPI3mBmKIeu68icvs2g5MWGE9E6p1QANOlqkbMa/ktzfdO8gHPSVAayNuMO8JcS3VJnnZ4K+hhCtRPiFB34+WFS+0K5BBY/7GYFLUwe5qX8DfD0nxhGaFXms2pGInPpVWRAYkr744DCSb/SEsWhbpINPuIdH4XbaCwb+CR++8tmBvZ+j26EGC9Wf6nsZcnK772nh79jXPBPq1RMFzn7VAlSpGyD95xh0QdG43GVJNIwRN80ggXNmp4c2rLbGqsgA98B8BTPIWj+vc9ppTvc7bDfkElUlho7GGFNdwupxhzTY6wWcxzFQ4mJJ3tooj4A4iGBzLPMozoOZxp/IpU14//ouIBuUwS6DMyoH33Rv9yycZFkv3EtUcQXkRxElFg7Zdg4NhUNQyqTCj50G9LRAzOyWAqTMmS5kOrkPRaNpcgXz9CrRAtfcnVkJR2DgCsiXeheOP8sLmPcl1iM40HWTNqlO5P7ggvoYr4987odSbvx8MMk8hh4k9nfYgSU+GnjVLYejI6nzZpy3kDLt55IcYLY/EZLlsCCrqLkVpIw==", "OTZbW7JEd7zPXtYl3O62ij/q+S8fq3FNTRWJS4UlSkI="));
        System.out.println(decrypt("/w9WTHDHLWrpuyLIjqlFNQ==", internalKey));
        System.out.println(decrypt("cXTKIrBTYHj3brpcgupSAw==", internalKey));
        System.out.println(decrypt("DSa1ouKnu2/A9B/r9pKDKg==", internalKey));
        System.out.println(decrypt("EGTZkWIKXqurU+SNTL88dMfPDM4k6D8fTTWBj0jk2V0=", internalKey));
        System.out.println(decrypt("pdLaRM5O8uTxXGgwYVZltvFS9pISH/yN+psg1cjxQ+rRU5KfFLBsSF3XpcDy0QBt", internalKey));*/

        //System.out.println(decrypt("IuCcdHXhNhQ2RQCjhAI7tw==", internalKey));
        /*System.out.println("\n" + decrypt("OpsPZBmrOeAQ+MkVMkYw7cu7yhBj4VsVxByag4k4OHJDXyejX/JKI1w0hlr6OK0d/d9sz7rcqW3fGiHndyenMFR1FHqJ55DcaJaqhYVxygKzDufqivooSzAxJTvKvxHwEkMLx7sy1HWI+tODW4PhXmtMVLPm7y1K8+n191eQLQ0UZEiGIVl5i7j+gWO+nibuONpTcf5IZXXUpCeDNClw77BJSH5JRo7iO258i0PthtMtwo1ySseVTAtUkjSq8V8J4GPcbwcsX7ga4+as4V8q5w5Q9fd9yOB8HX7bS1B6e1nSA1GQNojEeoqm6sDaDfzVSQ1Y3+sEoGtav678k2Yb9YMN0TCTc8b5JT0OmN7y5m5xdtTUuoICBdVattc7ZIBv0XdfvYTuXc7dNmUmxzM0M23VPAPzlV+H+bxkDEQrMc2hFvY+Xg2T4sgCEo7WNl2fwQP1WwF/4PcG1qEeDPI0KdDYCieM8Lyxmoz5qxuUiUZOMxQzKMDNYKxqQT4MYAgYqcPW1x4jnogyWQQj5ZgNaraw0z8cb9ekr7mH/5oSoyaPLXJ2BviHOo+bdx7n3EFEMwQfzmXBGwwf+B29Bs2w0ge14TIHyf+nKyT6E/WHkGo9RTuWuMOwGKGprcj/Wh1rR4fq6UmCrVQQs1tIrUI3q+7Xy/b6RSVn29sRKc+o7fZgH0q+gzcqtZvd8qZMISHU9ir641y9OZPDELKTGUDJr157HTn/jnYailpjSEjjQAQu4aDFwifqcdbIV2o/pJwVs9xs/GoB+cdJ3c6T3Zn6mSKVvBmjKSfBlorQ26itRH+ipT8gYBdxGA37DCoefG8N7tQb8dcg/BjPORsxqTZgTo+UpUEvolRdNBxpDvYUJIJP7xBTRxPRfrzM/kAPTiN+CrVwI2Ct9tpakkYbeh4nmzB/s5x0AM1a1gsyJhzoEmXw6xdzLMeT7sXanTm7rOZdXxHIarj2PxECk9PmQrYvhnXQsosuMOmy77BQGDjP/hSuxP3rQ/R5OUnXNNvJ331KhjDmPmvCiumD8mRTgorVISY/Dm0N0XjNUJx7vdhTjvr06LzvDRaXOaWHG3RMP7//Q2PNhVNfrjM3nSXYaqPxkMKtr8NtqY+EaqW8xPe40HMjJTdXvbJoievi6MAXXCMmGzK+kjDqFDilherz4UuQgOogJr8+DARAX5cNNtWmuCXMjV5bO5U1BlJl8w/RE0mmMvDmqVUrBJvh3++PbJ/PxzMQboBgJSyPMY6zSr5HXX0WD0oS4RiFy0Bd4htjmL6yJk5xT7jWEGHUZdyfekc8l4CNStbLw4GFt7k+ehv1mNlR2AQk8ZpLpNx5XP3YlT4q4/NFw3JrqC6KHDhUWbieEhIKKcBXjS0wgViCsiW+3VEcUgj+xy1jGAaiyYBcdlZ43syLzz3hTXrKOEBEMUt1Ko7QH8cDZZhIkIjbdU+89OWP7XSgOLG22UX6cugFIf5UQfOA3tzrMe6GFg8C/i683yroqFgLq/CMzxa4CXSeburZVlK1vJsKvjUio2IeGrkrg851RPOhtS4cO1+rShT0m4yxKWgtn+0zlx0O4hl/57Lb8qjAqfvQQJwJzskMbtbv8KjHEStnEtv1FzwA9PkC9D4/aw04WFOp9nDEH8zWWKNZGzrbR+67reOKKmBZSCVIzlrKS+f8le/qPdGQ6hBzeVlcVKAV1ayvfuB1OiaFETrOvAjUTFafAdPW7BxgQP4xQz2uE2B1GJzH9Mn2Oabl3fCvRSUWRrCJgDUVIO29HLs8VAAU9k8lj+sp9ybohxci4js0OXDEjnIeqLUFWvAPuWZ2yXRQ88Esrv0Ikco5Vq+u/4EE8kFCwQJy9QyH7AHReZ8I/ahAFd+P0AI7+FkvPJ4bPPq3PhcRLCET5dN/BiJX2BAmsLlbHDYV83tbEbKMA1N+VnXf9JnbmmdjNMq/+1n6rzddzwSs76emdxb/YPN4OZEY2cL0LD6RS5EMeKTmnvc4BVFGeXOg1+gQfA8mNK2kEZFxvMB80C5AUcwyWORZU3yVSm7DcxKwMQucXfCA77pnQn+rnbt2r5PNBusB2Q+lFhWWraeXUPhf+uT2k1LC5kfASyMICuOjbdskC0BnlCraNEFc280NyGjtUA3mCoo+zJGbKHXMnFv8ZuyZFi58w3B5ppSYF62tOwWUXM1PEN3IP2jLjxj0MwqH17nn36wjlVWm3AthTbyeFFgul7kFT0OKcgeXtqYUqAUkZimOEBJo5OfyV+rwdNOL9KjVNexEOD3coOtrngVtaHzVrwr0XLAa+UYEY2+TtLIyz2BmdhvRcUyAOMUGIFe8NvaCrGHg4PtM141pDDc4hqmWDZe7lABtZtkzJkHHizEFJI5D", internalKey));
        System.out.println("\n" + decrypt("SxdIacsn4wXzYeju1uPTmVxn1HMcfiCFs08hOdof+i/l8xSBOpqWP3zTauBJoaaRtLiVncg4xNxz/ObNnumfXKcmCkUgTxWn5ZB0dO9x86tj7BZeUQtC5mEc2KzBHvmQqwWifGi1pbB76NV2+ncQa32/5lVhou6Cgue34oHZ8UJL97yl49DFh2YBuFGbE6OBkk+QZ9EjwukHupnvjy54efuUulFmP/qoOILw1p3OAsq2iCwyZUD9uG1dduz439LEorXMW9us4TUxvKjUzu6Ci7MezK+UGg+D2qOHS+gGGLTKDnc+H1P6UhIEGEh5WApX/Syy2YM37K2zzSZF19ZDXcEPsXzM2waIV/j2IiYDHUGiSCjSYBMkwAQ0SxckipUcPs4msvEHQx+3qdenbidnYCc/xkwaUnX8L1gDz9Pt3WVHzbkidSI9hj2UO0ZMM7u+icXbXAoOQNpMWcglRFX9m24j6SbVdwQ4vh7C33pAHbmyQZz8cA2HrLafwZmX97flWs4ZSGVmHeSO4W6+q/J/v9pdsSJ76xaB6fPMApRbYxWvJAMCh9CxqCa/WTwoPIDz5nsYiHD0ZsUesCfzO8/MjojP1XJssidcSZ/fBQ5cWT1xwgZEBsSwfqp8qCP0TkfSaeNO4WRK0gdTxv6id8n5mJlQQPvaDC0RZU5jCXnI3u2CIoNErDPSykgzmI0kqdCsZO9eyeIvbihajfg7oof2IEyZyDXUl3zVtxZyeAS0xqMn0AhAq6tnMwG20VRMqRgPSUCD9zo0ANR7in9jghJHq++vqKSAgqg6OPLSsPmnum3Q+6sNlqhKcDMaCHU/JiIT8oA83Kf8kLzTcfXj9/C1AUuF9l9L0QmA1/9+cAiXCe7w+tJs8QHpE5B8ffiHe4/IjghmLZwrtnhrqu9H6Q2Wsa3ZjIHTRlpu6jCksMRM9ttDrcVmnpMc4pZO/bpnPkY/uUbPvlodqdkxZTtuW3BPGg/AMQMFg1tuitd/JD9aTEDYsI93GBL8sAPtsOjc89BvJRUvDkvac1D0npUfGNqJITiID7F7Q5Fe0iNxdg0qUyYxLuZEqGVvghrKa0qPg2pVOgsovA+hLiJ8UQ7YCB7S6FFORKzRCIqk7MFkM58FrpGyUFIeSvriuSMhJcX9Oe8vDupeRODWFEAPcx1ci1FkmxpIc6nvCcofmaZd5D9mYgN9yIiVWI3Kmh1/9QFdUDnHTZFMDp7zgk2ToZMH60I+djmBvY9+c3fkdX1flQKb20S3v4r2KQOV4nXRfvwp6mbpMqsoZwgbSp++dV9KYngeb/ijDnfS1UzitjIVsoWCN45NgTBaMuWlQ+SAEDbTEzOjQ6kvTbPFhlQQjL3bbnakOTNLlYVo1mXIiaZtEJAZVitc9TOc9ADTSbu8qgNuZngYB3gDVWC7/imU8jCi/roqawfk8B73YrO/U5ZX29+MUiA9iCd+LzuJo3toKSGFiCFLYOdNlW+ZDwMTJqDWZuhs5cSIPYV8k6Ip2KO4OPfoU2dGoe6IEM66TKBm72rN7wfbN4+24wLFijgBf0Gj6ZWESUN70H85v2opPMicAMyipBF69NGRK58mu+HEkbNLbuBUu9bVEl8hArdcxuhlUB4VmE6WrI3FwwccfBX4i8vtZcp8WmDvJMPK/J91aNy4LdcCVCEig9lziW+zxZVTLzy18ng1Q6zoy9hG3S/jv9S8MuXGSfVC5ShSaBS98yXrgX3LI75ZW2dh3h82ZtNz6gzYvW7pRTscGXgRmZC0v/YSQ9M49Hi6edPpkPjhF4QFMMRUQR/QIzEYf+geMN8b9RPYfhtKca8o3Jz9LkK3UOPpfWZUhwuKPiDA92AXLD9y60WyPypNn9EzEPCtrfss4HLK4tPvKG5peo9Fgs4DPFxN+R4l8DCsj/Ke6ZlHxSKG/YsmR3GZN2EFsopA0TGr8TY9qQoBPgy1jiJzbd64BEIHurU03/8/KtBJ/LJoF7xyHHGT", "KXjxCR5OagliWDjCF444iEyUPZg08QPtTBq6JPhDTiA="));*/

        //System.out.println(encrypt("paygate|202105250008|612202ab|1|IND|INR|SALE|http://localhost/safexpay/response.php|http://localhost/safexpay/response.php|MOBILE" + "~" + "85|NB|7|7" + "~" + "||||" + "~" + "Ashish Gupta|ashishg@safexpay.com|9076099020||Y" + "~" + "||||" + "~" + "||||||" + "~" + "||" + "~" + "" + "~" + "||||", "PE5K0s/KBPfwyxRDxWW64nRxdYMHIaAuQIRBCl7LyaE="));
        //System.out.println("\n" + decrypt("fooen804sOqgcaJdT1fNI38+3VcZ4oLx34n1bFRV7TwM+59oQG6LM14LVQNY8xmYIl7+DgOrmXY5LdtH8j2lMn+YOBRVcxNWkhuzJogu7SvvlvZPSXLPJI4kv8yXg512c9yo9Qw696ByPwZlb8ZHArJ87OcaAw+UgOa9El9Kh/wwx+zvqqy3mrbamp9eanXoRWMw5SpQSAOuYQ3yl07PujcXKyKbYnYEpFI0yWftKRD848ay789OvefI1E9kab7fpY0OZba2ND0/bj2fqoIabGIhVIwmPEjILzkj1AP/fIVVZSmij7gf1LwAYvvphjFf", "PE5K0s/KBPfwyxRDxWW64nRxdYMHIaAuQIRBCl7LyaE="));
        //System.out.println("\n" + decrypt("fooen804sOqgcaJdT1fNI2afmWgPmJulzerCqS1bAFF7oh0Ibsc6xlhDyK+X8S7lvoXdZ3BmoHoJh+XnlhJMUQNdudn+EBvrD73gZ/n8yQKKwDMdf3OkuojQNxOvWQbUM2ajHkC7pQ6f+co2kp1fiK+9e84Ak0sta5E+ydOIiNA", "PE5K0s/KBPfwyxRDxWW64nRxdYMHIaAuQIRBCl7LyaE="));

        //System.out.println("\n\n" + sha256Checksum("202105250008~612202ab~1~IND~INR") + "\n\n");

        //System.out.println(encrypt(sha256Checksum("202105250008~612202ab~1~IND~INR"), "PE5K0s/KBPfwyxRDxWW64nRxdYMHIaAuQIRBCl7LyaE="));

        //endregion
        /*System.out.println("enc : " + encrypt(
                "paygate|202105250008|983|1.0|IND|INR|SALE|http://localhost/Performance/public/simApp|http://localhost/Performance/publi/simApp|MOBILE~|||~||||~Nagendra|nagesh@safexpay.com|7710910181||Y~||||~||||||~||~~||||"
                ,"PE5K0s/KBPfwyxRDxWW64nRxdYMHIaAuQIRBCl7LyaE="));*/


        /*System.out.println("Decrypted MID: " + decrypt(new String(Base64.getDecoder().decode("Q20wOEdsaWlERy9STVpnUHpaSFV6Zz09")), internalKey));
        System.out.println("Encrypted MID: " + new String(Base64.getEncoder().encode(encrypt("202105020006", internalKey).getBytes(StandardCharsets.UTF_8))));
        System.out.println("Decrypted ORD_NO: " + decrypt(new String(Base64.getDecoder().decode("WWt2SGQvVUVyUFAxYzY1RnA1Tk5qQT09")), internalKey));
        System.out.println("Encrypted ORD_NO: " + new String(Base64.getEncoder().encode(encrypt("43437", internalKey).getBytes(StandardCharsets.UTF_8))));*/

        String str = decrypt(
                "6z+YAnOgIHAdW9n8rhZcvhiWGc+SSBGxbbTrmB7CeKVJa7MNN3wItBqBJuHJhwotAXciVvcCXw+5NqxFIvXn+9L+LF6R03vDokegmH9xzjyKfVc2oePOjrPb10Y25fx8cGDOHhVajBydY8hjX+u089qY9betqM3O/KL1QmLTMKI87Y/65vSsVQKnlnVdTB3iz4/gWy/+bbHXVlQofYY4Lg=="
                , "+6nCluPpB5BnOLLopZqfquMv104yAd8Wd9fEF6oY+GI=");

        byte[] bytes = Base64.getMimeDecoder().decode(str);
        String as = URLDecoder.decode(str);
        System.out.println("Safexpay --  " + str);
        System.out.println("Safexpay --  " + new String(bytes));
        System.out.println("Safexpay --  " + as);

        System.out.println("TEST : " + decrypt(
                "0fcNIbUOafnPY53XUKPLdoi2wCxm43mqw9nxUkjrU4S/Kdeu742fdXJyG7YtroWn3p3PaukKvSM8kiNo6cLxFTaAIK8WknsK9KHdvThfyif8SgVu3G4ymJqoma1lBC3Iz4NT1H9M1PMzNWdyEGcgDkxDluGKv2hGQ3z6lZ+ZAuNZGHyhNQp/1VSvmGp4nUzZDIjyqhnB5W+Ounl1q1LGQAB+Mr/LYXlVsKMQreJQ/40avIeq5fC5QVFKScXDL6bD5qGC057OoLzC5klNzsnyqMdEQzSoP644HhEibZAdWAclT1htVHL6eQblNkVNxRZcGK2G4H7lEEDX5RdZ4nH0JWA78FsabpT2rEulbvky6eFM1vPf+QsOS1yPo5zP7enhnQ1Hu92xD8dnxQmaMuK9CzOy6c6/k7XFmdG7pYJVsgftdAqlHUlfbTndQzaJf7qCv6qjpQyN0GsgIsEQU/MGEDYExEkDPuapBc6jkaUYH4JLsntYA7syLjHE343UftC8PcZZflCcDeQ7RATIGR48f7mFFJm101UPRe5LkZ8XnY9YAz5OqYPgBG6rFhtwZ81XnLZ8D3aLHfeiG1quPJkC19Dns9FaZinAMkvCUW2AEK4O0igalpywDaGO/u7Ykpl3tQTzFegPLv008QPlWQadlKkQ10hTT2vgGay9EYyPjW62ABhoJSmjO+tq6wVR1pE5wtZ8n63UY6SwzREoTC8uBew6zxm+Lzr4alZHm/4r9m9OTKLd/lRt1DX2DqoZ/qyp5cHYNw68VrEGigyFcwBM6upv/uEKUkRSdC8d7hCNBO4doJRoAgkStl4z2BgwRI17+Ay27GTyxMOnBjfvVfBCFUuLZN0Wg0KDszAPe2Vw6FC+HNfIUcTYwRQXEi/+boVw1mB3WjwIj3O5m9DZKS14Zxg4/qpmaZjEL7L9jK4GMr8jxABjm1OpZ5QA/S/PA0PZrHTumOddqtvKSxWC4syyFeppGAoLRa88aFr+lfQ4gfdXAw+llkSliJDar++Rmm1Ljq8OSyEDNu1oI2BRALxd3mBfsjcO5Fp4tXWwa6gHdvRzbtJhN8uSbrFQ0Kn5D0Dn6atGbILyXAJyvZVfwMnLDS8US8Bq25KBruAv9Iwqe47W0Pm3fM5yt12Y/0fWkxlrhlppLualUcL23c0MPRZNLORZ65ZrjOjBybPNSV1CDB6KvHfOEBwn10BWD8kSsepyJiaCMGAeC1x8XgzfB+fXzLlnpoDP+NpStIC5UEoMdOscSTrVXpLWv5lNuHwxYY9vGe/ovUqOEWhiN14M0d+HDBNBNxC686kIPqncJwzcjOEghTAZNfttRVMyiLrFJNtPhEy6AmEvGq5w28jm/t5c4mtdRR2jmEyWH2Jb7Zc/2nX2XKzAzGtM/yX4a4YpTCuuWyNEyQr4h6hLKRRfanYqij0vBjQLBvOyLp2QjtwaWLgLKmL/ViAsjJTrvL0oOCd+eN0CdkGyB7NCXiWZZAFI5Z5dwTGoQzzC9zl2IFNTs1b1Gj/BWaEuJwL9gONas6rMbaLD0TEpiKzaJH2aAdFFc3dyS9Ef++kFWEl8xSXjqaV8/OvqlkTFJYhLqLRht2Z33nkIsosC7njOb6HdD4xxUlk7iyGSMEHulh0EdjymU0AKYUFPbrY4dC23J8OZMU+iuujho3QvHRCC4xJ8zaS7KSyWUP7Cw8geuhvtWc17y2KE8AhBuw9z6mkKUzbWc3GqPfMVq+LYcMCaSkUyQjmarO0ibhz83XVkhTazo0f1zjnYowFte+pw4J7mog/V1E2KjtvhD2PWuqlIsM+V+w1BL/sBsdvCcoB1B4Ek6UrWXsWnPPeA5kjcHqo75+aijFNVzdxS4w6k1c4WYcf1fB61F8uyZGPb9Bz+F2rDOYlexIaHibxlF8db9kM+3xDtKkqaw2ivk8/WodsBa8NxFxHBMWj48iYuI66MvFsmnDWVDfaK8LTPbjzxKfnetFgjPUjS1qi/tu50rhiKyhsCPN55SxeYyP30AxeiP4CADZ7FMmn4qF36maGoujkRskbAY61b/eOPSLo+YAgP9ZhJp6695hlFnt/VFYsnOrwtsQVttZFX5FjAxuosmwEEyCiG1OijeCi+aVzEfyh2QmHMn5sQDCTuyRSTY2xaQnDFa2NUAYG32TEdUOE0EHhjp4B8PvnjodyFigXUDp8ApDbLEel+8Oiql/+w0Xn8u2LOKL0LD+0="
                ,"9tKCR5Rdu1C6AcIPjSLAh5WUh7EOjqnUSxnXgtVO6yQ=") );


        System.out.println("TEST : " + decrypt(
                "sGpT2m7eHtOP3fDpEiYEGUoWKy/GpV/z2ODBr94WoBDIKFCQnWNjtI+pZqXfAYiEtrnxPygi006r/sdSw165HBY/cQD2k1rCBXleQy+iSFcD/SS4jAQoT6+YmF91MwTTLIiOL9F+sJjB1tv8i78tZMJ9TrfPx1VFFg7yz2UTg1xSJe0dnXCEt+1wUPVyBSKO0rjo09IO/tnEHlV2z/uvYIzyR50iAIwMOc/y8MdaykAvuGBQhaZ2aRQTVM7ljRRg"
                ,"+6nCluPpB5BnOLLopZqfquMv104yAd8Wd9fEF6oY+GI=") );

        System.out.println("TEST : " + decrypt(
                "H/t2F6aAiCgigz1N44Z6zwgLXQ3g9CKDIGJenb+E5700Uh5T6Zka8mocPHMSMoSL23SbgYJpq1yvKNwODoPVUyC1mxCw/vaTBoGGlt0etVXXS6+EWzarS2TDPejvwdiIAQSu9HXgrF+xfVuQpQwkXaehW0DgCXc0B8+72oEJmFCFFjcsr21FSSx6y890U71xYk6N6sxmPkAFjwhg3nJjK+ks+UIj8Zox5syrtpWAePLvZH4T2Ea/Z5HxN4Ilj4w8+38z5b2SrZNeGvwO6RubYywOZz+CDdB0AfxTfV8hSNZV2K4JcQ2F9IZ6jzJ4MqgM"
                ,"9tKCR5Rdu1C6AcIPjSLAh5WUh7EOjqnUSxnXgtVO6yQ=") );

        /*System.out.println(decrypt("76DWd91lLt4DksRzbZyquHJ1efR4uNKI3gCbI7gIO/hU/FejwMlnygMsA" +
                "JKazwWXiGHAuMInIvKOxLK0CHC8SQjDNg2Gl7HOw2JMPJ3t9QR5y3P0nv6sLyLPTarpcjwEKAJIxDShExee2G" +
                "6nWxxjZgSatdaa3qTUjRzua5XvAl++umr/LhTGPSuuS3OExZKUwyAT4nXMMz3LLlQyDVGDKnBPR3SJLFGchd" +
                "EI4J8fIyFwhpEf/TPKJ6+qMCKeOY4qzxpwj17GDwHQE18EKF3MQozPk4/Uv/55I4lrjB1lDU+yLz" +
                "9YCzy+6/OlnL6KRfKXNJJHiitOZk5kMlu23TxFVvQVdUUKA+qcrkAJAKkmFlpJdYKaUZGVIAyxdAQ9Zo3+" +
                "0ukG53/qe+qHi8SVkCZSHmfcTZ3W81sOLZihET31Zt4J/x7ARsIaz1y+4ITxIE51U654OnO6NtpbGvmwh" +
                "AmQ1oDhV9jJv28Qi5PopYG6ibzgjACz5+JytxFF29RtEjnEHMEm95i5H8zTm65ms090NvZvPaF5uhegjXw" +
                "BfgK2BIE2Va45rT/WN2AaX+MSC6C+aIf/Dv1w+I6kPG6jfgZNr6MCsw0/f1SdyGIbGIgSVcnCShj4xsv3fmdz" +
                "3Znm90eXCWx2WQSKL9Q/vi6UikcoRA==","PE5K0s/KBPfwyxRDxWW64nRxdYMHIaAuQIRBCl7LyaE="));*/

        System.out.println("\n\n"+generateMerchantKey());
    }


    //region crypto methods
    public static String sha256Checksum(String base) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            //byte[] hash = digest.digest(base.getBytes("UTF-8"));
            byte[] hash = digest.digest(base.getBytes(StandardCharsets.UTF_8));
            StringBuffer hexString = new StringBuffer();

            for (byte byteHash : hash) {
                String hex = Integer.toHexString(0xff & byteHash);
                if (hex.length() == 1)
                    hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public static String encrypt(final String textToEncrypt, final String key) {
        try {
            final Cipher cipher = Cipher.getInstance(PADDING);
            cipher.init(Cipher.ENCRYPT_MODE, makeKey(key), makeIv());
            //return new String(Base64.encodeBase64(cipher.doFinal(textToEncrypt.getBytes())));
            return new String(Base64.getEncoder().encode(cipher.doFinal(textToEncrypt.getBytes())));
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String decrypt(final String textToDecrypt, final String key) {
        try {
            final Cipher cipher = Cipher.getInstance(PADDING);
            cipher.init(Cipher.DECRYPT_MODE, makeKey(key), makeIv());
            //return new String(cipher.doFinal(Base64.decodeBase64(textToDecrypt)));
            return new String(cipher.doFinal(Base64.getDecoder().decode(textToDecrypt)));
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static AlgorithmParameterSpec makeIv() {
        try {
            return new IvParameterSpec(ENCRYPTION_IV.getBytes(CHART_SET));
        } catch (final UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static Key makeKey(final String encryptionKey) {
        try {
            //MessageDigest md = MessageDigest.getInstance("SHA-256");
            //byte[] key = md.digest(encryptionKey.getBytes(CHART_SET));
            //final byte[] key = Base64.decodeBase64(encryptionKey);
            final byte[] key = Base64.getDecoder().decode(encryptionKey);
            return new SecretKeySpec(key, ALGORITHM);
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String generateMerchantKey() {
        String newKey = null;

        try {
            // Get the KeyGenerator
            final KeyGenerator kgen = KeyGenerator.getInstance("AES");
            kgen.init(256); // 128, 192 and 256 bits available

            // Generate the secret key specs.
            final SecretKey skey = kgen.generateKey();
            final byte[] raw = skey.getEncoded();

            newKey = new String(Base64.getEncoder().encode(raw));
        } catch (final Exception ex) {
            ex.printStackTrace();
        }

        return newKey;
    }
    //endregion
}