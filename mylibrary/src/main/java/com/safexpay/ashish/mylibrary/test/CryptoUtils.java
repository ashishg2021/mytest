package com.safexpay.ashish.mylibrary.test;

import android.org.apache.commons.codec.binary.Base64;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/***********************************************
 * Created by Ashish Gupta on 30-03-2021 
 * Mail-ID : ashishg@safexpay.com
 * Organization by SafexPay
 ***********************************************/

public class CryptoUtils {

    private static final String ENCRYPTION_IV = "0123456789abcdef";
    private static final String PADDING = "AES/CBC/PKCS5Padding";
    private static final String ALGORITHM = "AES";
    private static final String CHART_SET = "UTF-8";
    private static final String CLASS_NAME = "javax.crypto.JceSecurity";

    {
        try {
            final Field field = Class.forName(CLASS_NAME).getDeclaredField(
                    "isRestricted");
            field.setAccessible(true);
            field.set(null, Boolean.FALSE);
        } catch (final Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Encryption
     *
     * @param textToEncrypt for which text we encrypt
     * @param key           for which key to encryption
     * @return encrypted value
     */
    public static String encrypt(final String textToEncrypt, final String key) {
        try {
            final Cipher cipher = Cipher.getInstance(PADDING);
            cipher.init(Cipher.ENCRYPT_MODE, makeKey(key), makeIv());
            return new String(Base64.encodeBase64(cipher.doFinal(textToEncrypt.getBytes())));
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Decryption
     *
     * @param textToDecrypt for which text we decrypt
     * @param key           for which key to decryption
     * @return decrypted value
     */
    public static String decrypt(final String textToDecrypt, final String key) {
        try {
            final Cipher cipher = Cipher.getInstance(PADDING);
            cipher.init(Cipher.DECRYPT_MODE, makeKey(key), makeIv());
            return new String(cipher.doFinal(Base64.decodeBase64(textToDecrypt)));
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static AlgorithmParameterSpec makeIv() {
        try {
            return new IvParameterSpec(ENCRYPTION_IV.getBytes(CHART_SET));
        } catch (final UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static Key makeKey(final String encryptionKey) {
        try {
            //MessageDigest md = MessageDigest.getInstance("SHA-256");
            //byte[] key = md.digest(encryptionKey.getBytes(CHART_SET));
            final byte[] key = Base64.decodeBase64(encryptionKey);
            return new SecretKeySpec(key, ALGORITHM);
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String generateMerchantKey() {
        String newKey = null;

        try {
            // Get the KeyGenerator
            final KeyGenerator keyGenerator = KeyGenerator.getInstance(ALGORITHM);
            keyGenerator.init(256); // 128, 192 and 256 bits available

            // Generate the secret key specs.
            final SecretKey secretKey = keyGenerator.generateKey();
            final byte[] raw = secretKey.getEncoded();

            newKey = new String(Base64.encodeBase64(raw));
        } catch (final Exception ex) {
            ex.printStackTrace();
        }

        return newKey;
    }
}
