package com.safexpay.ashish.mylibrary;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;

import com.safexpay.ashish.mylibrary.test.SplashActivity;

import org.jetbrains.annotations.NotNull;

/***********************************************
 * Created by Ashish Gupta on 7/1/2021
 * Mail-ID : ashishg@safexpay.com
 * Organization by SafexPay
 ***********************************************/

public class TestBuilder {
    @SuppressLint("StaticFieldLeak")
    private static TestBuilder mInstance;
    private Activity activity;
    private Context context;
    private Environment environment;
    private String aggregator, merchantId, merchantKey;
    private String orderId;
    private int orderAmount;
    private String orderSubCharge, orderProcessingCharge;
    private String currency, txnType, channel, countryCode;
    private String successUrl, failureUrl;
    private SafeXPayPaymentCallback mCallback;

    private String pg_details = "", customer_details = "", card_details = "", upi_details = "";
    private String billing_details = "", shipping_details = "", item_details = "", other_details = "";


    private TestBuilder(Builder builder) {
        this.activity = builder.activity;
        this.context = builder.context;
        this.mCallback = builder.callback;
        this.environment = builder.environment == null ? Environment.TEST : builder.environment;
        this.aggregator = builder.aggregator;
        this.merchantId = builder.merchantId;
        this.merchantKey = builder.merchantKey;
        this.orderId = builder.orderId;
        this.orderAmount = builder.orderAmount;
        this.orderSubCharge = builder.orderSubCharge;
        this.orderProcessingCharge = builder.orderProcessingCharge;
        this.currency = builder.currency.equals("") ? "INR" : builder.currency;
        this.txnType = builder.txnType.equals("") ? "SALE" : builder.txnType;
        this.channel = builder.channel.equals("") ? "MOBILE" : builder.channel;
        this.countryCode = builder.countryCode.equals("") ? "IND" : builder.countryCode;
        this.successUrl = builder.successUrl.equals("") ? "http://localhost/safexpay/response.php" : builder.successUrl;
        this.failureUrl = builder.failureUrl.equals("") ? "http://localhost/safexpay/response.php" : builder.failureUrl;

        this.pg_details = builder.pg_details;
        this.customer_details = builder.customer_details;
        this.card_details = builder.card_details;
        this.upi_details = builder.upi_details;
        this.billing_details = builder.billing_details;
        this.shipping_details = builder.shipping_details;
        this.item_details = builder.item_details;
        this.other_details = builder.other_details;

        //ServiceGenerator.initialize(environment);
        initiatePayment();
    }

    private TestBuilder() {
        //this is require
    }

    public static TestBuilder getInstance() {
        if (mInstance == null)
            synchronized (TestBuilder.class) {
                if (mInstance == null)
                    mInstance = new TestBuilder();
            }
        return mInstance;
    }

    private void initiatePayment() {
        context.startActivity(new Intent(activity, SplashActivity.class));
    }

    public Context getContext() {
        return context;
    }

    public enum Environment {
        TEST, PRODUCTION
    }

    public interface SafeXPayPaymentCallback {
        void onPaymentComplete(String orderID, String transactionID, String paymentID, String paymentStatus);

        void onPaymentCancelled(String reasonMessage);

        void onInitiatePaymentFailure(String errorMessage);

        void onInitiatePaymentTimeout(String timeoutMessage);
    }

    final public static class Builder {
        private final Context context;
        private final Activity activity;
        private final SafeXPayPaymentCallback callback;
        private Environment environment = Environment.TEST;
        private String aggregator = "", merchantId = "", merchantKey = "";
        private String orderId = "";
        private int orderAmount;
        private String orderSubCharge = "", orderProcessingCharge = "";
        private String currency = "INR", txnType = "SALE", channel = "MOBILE", countryCode = "IND";
        private String successUrl = "http://localhost/safexpay/response.php", failureUrl = "http://localhost/safexpay/response.php";

        private String pg_details = "", customer_details = "", card_details = "", upi_details = "";
        private String billing_details = "", shipping_details = "", item_details = "", other_details = "";


        public Builder(@NonNull final Activity activity, @NonNull final Context context, @NonNull final SafeXPayPaymentCallback callback) {
            this.activity = activity;
            this.context = context;
            this.callback = callback;
        }

        public final Builder setEnvironment(@NonNull final Environment environment) {
            this.environment = environment;
            return this;
        }


        public final Builder setOrderId(@NonNull @NotNull String orderId) {
            this.orderId = orderId;
            return this;
        }


        public final Builder setAmount(int orderAmount) {
            this.orderAmount = orderAmount;
            return this;
        }


        public final Builder setTransactionMode(@NonNull @NotNull String countryCode, @NonNull @NotNull String currency, @NonNull @NotNull String txnType, @NonNull @NotNull String channel) {
            this.currency = currency;
            this.txnType = txnType;
            this.channel = channel;
            this.countryCode = countryCode;
            return this;
        }


        public final Builder setMerchantDetail(@NonNull @NotNull String aggregator, @NonNull @NotNull String merchantId, @NonNull @NotNull String merchantKey) {
            this.aggregator = aggregator;
            this.merchantId = merchantId;
            this.merchantKey = merchantKey;
            return this;
        }


        public final Builder setResponseUrl(@NonNull @NotNull String successUrl, @NonNull @NotNull String failureUrl) {
            this.successUrl = successUrl;
            this.failureUrl = failureUrl;
            return this;
        }

        public final Builder setPgDetails(@NonNull String pg_id, @NonNull String paymode, @NonNull String scheme_id, @NonNull String emi_months) {
            if (!pg_id.isEmpty() && !paymode.isEmpty() && !scheme_id.isEmpty())
                this.pg_details = pg_id + "|" + paymode + "|" + scheme_id + "|" + emi_months;
            return this;
        }

        public final Builder setCustomerDetails(@NonNull String customer_name, @NonNull String customer_mail, @NonNull String customer_number, @NonNull String customer_unique_id, @NonNull String customer_is_live) {
            if (!customer_name.isEmpty() && !customer_mail.isEmpty() && !customer_number.isEmpty() && customer_is_live.equalsIgnoreCase("Y"))
                this.customer_details = customer_name + "|" + customer_mail + "|" + customer_number + "|" + customer_unique_id + "|Y";
            else if (!customer_name.isEmpty() && !customer_mail.isEmpty() && !customer_number.isEmpty())
                this.customer_details = customer_name + "|" + customer_mail + "|" + customer_number + "|" + customer_unique_id + "|N";
            return this;
        }


        public final Builder setCardDetails(@NonNull String card_number, @NonNull String card_ex_month, @NonNull String card_ex_year, @NonNull String card_ccv, @NonNull String card_holder) {
            if (!card_number.isEmpty() && !card_ex_month.isEmpty() && !card_ex_year.isEmpty() && !card_ccv.isEmpty())
                this.card_details = card_number + "|" + card_ex_month + "|" + card_ex_year + "|" + card_ccv + "|" + card_holder;
            return this;
        }

        public final Builder setUpiDetails(@NonNull String upi_id) {
            this.upi_details = upi_id;
            return this;
        }

        public final Builder setBillingDetails(@NonNull String billing_details) {
            this.billing_details = billing_details;
            return this;
        }

        public final Builder setShippingDetails(@NonNull String shipping_details) {
            this.shipping_details = shipping_details;
            return this;
        }

        public final Builder setItemDetails(@NonNull String item_details) {
            this.item_details = item_details;
            return this;
        }

        public final Builder setOtherDetails(@NonNull String other_details) {
            this.other_details = other_details;
            return this;
        }

        public TestBuilder startPayment() {
            if (aggregator == null || merchantId == null || merchantKey == null ||
                    orderId == null || orderId.isEmpty() || orderAmount < 1 ||
                    aggregator.isEmpty() || merchantId.isEmpty() || merchantKey.isEmpty()) {
                throw new IllegalArgumentException("Valid data is required to pay with SafeXPay.");
            }
            if (!pg_details.isEmpty() && customer_details.isEmpty())
                throw new IllegalArgumentException("When you pass pgDetails then customerDetails is mandatory.");

            mInstance = new TestBuilder(this);

            return mInstance;
        }
    }
}
