package com.safexpay.ashish.mylibrary.test;

import android.util.Log;

import com.safexpay.ashish.mylibrary.BuildConfig;


/***********************************************
 * Created by Ashish Gupta on 6/1/2021
 * Mail-ID : ashishg@safexpay.com
 * Organization by SafexPay
 ***********************************************/

public class Logger {
    /**
     * Logs debug messages if DEBUG mode
     */
    public static void d(String tag, String message) {
        if (BuildConfig.DEBUG) Log.d(tag, message);
    }

    /**
     * Logs all the errors
     */
    public static void e(String tag, String message) {
        Log.e(tag, message);
    }
}
