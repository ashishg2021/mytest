package com.safexpay.ashish.mylibrary;

import com.safexpay.ashish.mylibrary.test.CryptoUtils;

import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    public static final String internalKey = "HiktfH0Mhdla4zDg0/4ASwFQh2OS+nf9MVL0ik3DsmE=";

    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
        {
            try {
                final Field field = Class.forName("javax.crypto.JceSecurity").getDeclaredField(
                        "isRestricted");
                field.setAccessible(true);
                field.set(null, Boolean.FALSE);
            } catch (final Exception ex) {
                ex.printStackTrace();
            }
        }
        System.out.println(CryptoUtils.encrypt("202105250008", internalKey));
        //System.out.println(CryptoUtils.encrypt("12345", "PE5K0s/KBPfwyxRDxWW64nRxdYMHIaAuQIRBCl7LyaE="));
        //System.out.println(CryptoUtils.encrypt("y9oKnn/v70AgoK+UXQsvaHqhCYlU90wPy73r6ygpK+8=","HiktfH0Mhdla4zDg0/4ASwFQh2OS+nf9MVL0ik3DsmE="));

    }

}