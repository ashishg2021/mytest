-printmapping "mapping.txt"
-verbose

-dontwarn android.support.**

-keepparameternames
-renamesourcefileattribute SourceFile
-keepattributes Exceptions,InnerClasses,Signature,Deprecated,
                SourceFile,LineNumberTable,*Annotation*,EnclosingMethod

-allowaccessmodification
-repackageclasses safexpay

-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider

-keep public class * extends android.view.View {
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
    public void set*(...);
}

-keep class android.support.v4.** { *; }
-keep class android.support.v7.** { *; }
#-keep class android.support.v7.widget.SearchView { *; }

-keepclassmembernames class com.safexpay.ashish.mylibrary.test.**{ *; }

-keepclassmembers class * extends android.app.Activity {
   public void *(android.view.View);
}
-keepclassmembernames class com.safexpay.ashish.mylibrary.test.TestActivity { *; }
-keepclassmembernames public class com.safexpay.ashish.mylibrary.test.SquareImageView{*;}

#-keepparameternames
-keepclassmembers class com.safexpay.ashish.mylibrary.TestBuilder{
    <fields>;
    <methods>;
    public <init>();
}

-keep class com.safexpay.ashish.mylibrary.TestBuilder$Environment{*;}
-keep class com.safexpay.ashish.mylibrary.TestBuilder$Builder{*;}
#-keep class com.safexpay.ashish.mylibrary.TestBuilder$SafeXPayPaymentCallback{*;}
-keepattributes Signature
-keepattributes *Annotation*


-dontpreverify
-keep interface com.safexpay.ashish.mylibrary.TestBuilder$SafeXPayPaymentCallback{*;}
#-keep class com.safexpay.ashish.mylibrary.TestBuilder$**{public *;}
-keepclassmembers class **.R$* {
    public static <fields>;
}
