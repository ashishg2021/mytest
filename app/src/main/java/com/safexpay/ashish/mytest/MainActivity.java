package com.safexpay.ashish.mytest;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.safexpay.ashish.mylibrary.TestBuilder;
import com.safexpay.ashish.mylibrary.test.Logger;
import com.safexpay.ashish.mylibrary.test.TestActivity;

public class MainActivity extends AppCompatActivity implements TestBuilder.SafeXPayPaymentCallback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Logger.d("MainActivity", "Hello World! : called");
        TestBuilder.Builder builder = new TestBuilder.Builder(this,this,this);
        builder.setMerchantDetail("123456","123456","ab1jdh3bdk4jhd4")
                .setAmount(1)
                .setOrderId("123456")
                .startPayment();
        //startActivity(new Intent(this, TestActivity.class));
    }

    @Override
    public void onPaymentComplete(String orderID, String transactionID, String paymentID, String paymentStatus) {

    }

    @Override
    public void onPaymentCancelled(String reasonMessage) {

    }

    @Override
    public void onInitiatePaymentFailure(String errorMessage) {

    }

    @Override
    public void onInitiatePaymentTimeout(String timeoutMessage) {

    }
}