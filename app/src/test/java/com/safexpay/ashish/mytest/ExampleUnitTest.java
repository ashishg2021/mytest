package com.safexpay.ashish.mytest;

import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        //assertEquals(4, 2 + 2);
        {
            try {
                final Field field = Class.forName("javax.crypto.JceSecurity").getDeclaredField(
                        "isRestricted");
                field.setAccessible(true);
                field.set(null, Boolean.FALSE);
            } catch (final Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}