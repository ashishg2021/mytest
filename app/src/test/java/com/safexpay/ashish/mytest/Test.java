package com.safexpay.ashish.mytest;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/***********************************************
 * Created by Ashish Gupta on 7/23/2021
 * Mail-ID : ashishg@safexpay.com
 * Organization by SafexPay
 ***********************************************/

public class Test {
    public static final String internalKey = "HiktfH0Mhdla4zDg0/4ASwFQh2OS+nf9MVL0ik3DsmE=";
    private static final String ENCRYPTION_IV = "0123456789abcdef";
    private static final String PADDING = "AES/CBC/PKCS5Padding";
    private static final String ALGORITHM = "AES";
    private static final String CHART_SET = "UTF-8";
    private static final String CLASS_NAME = "javax.crypto.JceSecurity";

    {
        try {
            final Field field = Class.forName(CLASS_NAME).getDeclaredField(
                    "isRestricted");
            field.setAccessible(true);
            field.set(null, Boolean.FALSE);
        } catch (final Exception ex) {
            ex.printStackTrace();
        }
    }

    @org.junit.Test
    public static void main(String[] arg) {
        System.out.println(encrypt("202105250008", internalKey));
    }

    public static String encrypt(final String textToEncrypt, final String key) {
        try {
            final Cipher cipher = Cipher.getInstance(PADDING);
            cipher.init(Cipher.ENCRYPT_MODE, makeKey(key), makeIv());
            return new String(Base64.getEncoder().encode(cipher.doFinal(textToEncrypt.getBytes())));
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String decrypt(final String textToDecrypt, final String key) {
        try {
            final Cipher cipher = Cipher.getInstance(PADDING);
            cipher.init(Cipher.DECRYPT_MODE, makeKey(key), makeIv());
            return new String(cipher.doFinal(Base64.getDecoder().decode(textToDecrypt)));
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static AlgorithmParameterSpec makeIv() {
        try {
            return new IvParameterSpec(ENCRYPTION_IV.getBytes(CHART_SET));
        } catch (final UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static Key makeKey(final String encryptionKey) {
        try {
            //MessageDigest md = MessageDigest.getInstance("SHA-256");
            //byte[] key = md.digest(encryptionKey.getBytes(CHART_SET));
            final byte[] key = Base64.getDecoder().decode(encryptionKey);
            return new SecretKeySpec(key, ALGORITHM);
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
